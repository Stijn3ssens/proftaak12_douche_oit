const int IN1 =  8;
const int IN2 = 9;
const int IN3 = 10;
const int IN4 = 11;
const int IN5 = 2;
const int IN6 = 3;
const int IN7 = 4;
const int IN8 = 5;
int Steps = 768; //4096 or 768
int cstep = 0;
String TotaleString;
long timer1;
long timer2;
bool Draaien = false;
bool Draaien2 = false;
int interval = 0;

void setup()
{
  Serial.begin(9600);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  pinMode(IN5, OUTPUT);
  pinMode(IN6, OUTPUT);
  pinMode(IN7, OUTPUT);
  pinMode(IN8, OUTPUT);
}


void loop()
{
  if (Serial.available() > 0)
  {
    TotaleString = Serial.readStringUntil("\n");
    TotaleString.replace("\n", "");
    if (TotaleString.indexOf("A") >= 0)
    {
      Draaien = !Draaien;
    }
    if (TotaleString.indexOf("1") >= 0)
    {
      Draaien2 = !Draaien2;
    }
  }
  if (Draaien == true && millis() - timer1 > interval)
  {
    stepmotor(IN1, IN2, IN3, IN4);
    //delayMicroseconds(1000);
    cstep++;
    timer1 = millis();
    if (cstep == 8)
    {
      cstep = 0;
    } 
  }
    if (Draaien2 == true && millis() - timer2 > interval)
  {
    stepmotor(IN5, IN6, IN7, IN8);
    //delayMicroseconds(1000);
    cstep++;
   timer2 = millis();
    if (cstep == 8)
    {
      cstep = 0;
    } 
  }
}

void stepmotor(int motorInput1, int motorInput2, int motorInput3, int motorInput4)
{
  //stepp
  switch (cstep)
  {
    case 0:
      digitalWrite(motorInput1, LOW);
      digitalWrite(motorInput2, LOW);
      digitalWrite(motorInput3, LOW);
      digitalWrite(motorInput4, HIGH);
      break;
    case 1:
      digitalWrite(motorInput1, LOW);
      digitalWrite(motorInput2, LOW);
      digitalWrite(motorInput3, HIGH);
      digitalWrite(motorInput4, HIGH);
      break;
    case 2:
      digitalWrite(motorInput1, LOW);
      digitalWrite(motorInput2, LOW);
      digitalWrite(motorInput3, HIGH);
      digitalWrite(motorInput4, LOW);
      break;
    case 3:
      digitalWrite(motorInput1, LOW);
      digitalWrite(motorInput2, HIGH);
      digitalWrite(motorInput3, HIGH);
      digitalWrite(motorInput4, LOW);
      break;
    case 4:
      digitalWrite(motorInput1, LOW);
      digitalWrite(motorInput2, HIGH);
      digitalWrite(motorInput3, LOW);
      digitalWrite(motorInput4, LOW);
      break;
    case 5:
      digitalWrite(motorInput1, HIGH);
      digitalWrite(motorInput2, HIGH);
      digitalWrite(motorInput3, LOW);
      digitalWrite(motorInput4, LOW);
      break;
    case 6:
      digitalWrite(motorInput1, HIGH);
      digitalWrite(motorInput2, LOW);
      digitalWrite(motorInput3, LOW);
      digitalWrite(motorInput4, LOW);
      break;
    case 7:
      digitalWrite(motorInput1, HIGH);
      digitalWrite(motorInput2, LOW);
      digitalWrite(motorInput3, LOW);
      digitalWrite(motorInput4, HIGH);
      break;
    default:
      digitalWrite(motorInput1, LOW);
      digitalWrite(motorInput2, LOW);
      digitalWrite(motorInput3, LOW);
      digitalWrite(motorInput4, LOW);
      break;
  }
}
