//Laat het juiste verstuurde getal op de scherm zien
//De cases zijn gelijk aan de nummer dat het scherm laat zien
void showGetal(int plaats, int getal)
{
  digitalWrite(D1, HIGH);
  digitalWrite(D2, HIGH);
  switch (getal)
  {
    case 0:
      Getl(plaats, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, LOW);
      break;
    case 1:
      Getl(plaats, LOW, HIGH, HIGH, LOW, LOW, LOW, LOW);
      break;
    case 2:
      Getl(plaats, HIGH, HIGH, LOW, HIGH, HIGH, LOW, HIGH);
      break;
    case 3:
      Getl(plaats, HIGH, HIGH, HIGH, HIGH, LOW, LOW, HIGH);
      break;
    case 4:
      Getl(plaats, LOW, HIGH, HIGH, LOW, LOW, HIGH, HIGH);
      break;
    case 5:
      Getl(plaats, HIGH, LOW, HIGH, HIGH, LOW, HIGH, HIGH);
      break;
    case 6:
      Getl(plaats, HIGH, LOW, HIGH, HIGH, HIGH, HIGH, HIGH);
      break;
    case 7:
      Getl(plaats, HIGH, HIGH, HIGH, LOW, LOW, LOW, LOW);
      break;
    case 8:
      Getl(plaats, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH);
      break;
    case 9:
      Getl(plaats, HIGH, HIGH, HIGH, HIGH, LOW, HIGH, HIGH);
      break;
    default:
      Getl(plaats, LOW, LOW, LOW, LOW, LOW, LOW, LOW);
  }
}
