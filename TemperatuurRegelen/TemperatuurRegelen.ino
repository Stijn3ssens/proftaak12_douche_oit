/*
  Showing number 0-9 on a Common Anode 7-segment LED display
  Displays the numbers 0-9 on the display, with one second inbetween.
    A
   ---
  F |   | B
  | G |
   ---
  E |   | C
  |   |
   ---
    D
  This example code is in the public domain.
*/

// Pin 2-8 is connected to the 7 segments of the display.

//NULL, 9, 7, 12, 13, 10
//6, 5, NULL, 11, 8, NULL

//In de Smart Douche via achterkant van links naar rechts:
//10, 13, 12, 7, 9, NULL
//NULL, 8, 11, NULL, 5, 6
int pinA = 9;
int pinB = 10;
int pinC = 11;
int pinD = 5;
int pinE = 6;
int pinF = 7;
int pinG = 8;
int D1 = 12;
int D2 = 13;

int encoder0PinA = 2;//CLK
int encoder0PinB = 3;//DT
int encoder0Btn = 4;//SW

int encoder0Pos = 0;

int num1;
int num2;
int num3;
int num4;

long timer = 0;
int debounce = 1;

// the setup routine runs once when you press reset:
void setup() {
  // initialize the digital pins as outputs.
  pinMode(pinA, OUTPUT);
  pinMode(pinB, OUTPUT);
  pinMode(pinC, OUTPUT);
  pinMode(pinD, OUTPUT);
  pinMode(pinE, OUTPUT);
  pinMode(pinF, OUTPUT);
  pinMode(pinG, OUTPUT);
  pinMode(D1, OUTPUT);
  pinMode(D2, OUTPUT);
  pinMode(encoder0PinA, INPUT_PULLUP);
  pinMode(encoder0PinB, INPUT_PULLUP);
  pinMode(encoder0Btn, INPUT_PULLUP);
  attachInterrupt(0, doEncoder, CHANGE); //?? 
  Serial.begin(9600);
}

int getal = 30;
int valRotary, LastvalRotary;
bool aangaan = true;//begin op false

//Wacht tot er een bericht komt
//Leest tot een '%'
//Als het "Aan_" is zal het aangaan"
void loop()
{
  if (Serial.available() > 0)
  {
    String TotaleString = Serial.readStringUntil("%");
    TotaleString.replace("%", "");
    if (TotaleString.indexOf("Aan_") >= 0)
    {
      TotaleString.replace("Aan_", "");
      getal = TotaleString.toInt();
      aangaan = true;
    }
    if (TotaleString.indexOf("Uit") >= 0)
    {
      aangaan = false;
      showGetal(D1, 10);
      delay(1);
      
      showGetal(D2, 10);
      delay(1);
    }
  }
  if (aangaan == true)
  {
    SetTemperatuurRegelen();
  }
}
