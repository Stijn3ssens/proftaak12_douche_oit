//De tempratuur zal een omhoog of omlaag gaan als de valrotary draait
//De maximum waarde is 45 en minimum is 20
void SetTemperatuurRegelen()
{
  Serial.println(encoder0Pos);
  if (valRotary > LastvalRotary)
  {
    getal++;
  }
  else if (valRotary < LastvalRotary)
  {
    getal--;
  }
  LastvalRotary = valRotary;
  if (getal >= 45)
  {
    getal = 45;
  }
  else if (getal < 20)
  {
    getal = 20;
  }
  splits(getal);

  showGetal(D1, num1);
  delay(1);
  showGetal(D2, num2);
  delay(1);
}
