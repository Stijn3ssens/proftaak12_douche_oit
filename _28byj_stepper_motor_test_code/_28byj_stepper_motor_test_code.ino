const int Button = 12;
#define IN1  8
#define IN2  9
#define IN3  10
#define IN4  11
//#include <Stepper.h>
int Steps = 768; //4096 or 768
int cstep = 0;
int ButtonIn = LOW;
int PrevButtonIn = LOW;
long timer = 0;
int debounce = 200;
bool ingedrukt = false;
void setup()
{
  Serial.begin(9600);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  pinMode(Button, INPUT);
}

void loop()
{
  ButtonIn = digitalRead(Button);
  if (ButtonIn == HIGH && PrevButtonIn == LOW && millis() - timer > debounce)
  {
    ingedrukt = !ingedrukt;
    Serial.println(ingedrukt);
    timer = millis();
  }
  PrevButtonIn = ButtonIn;
  if (ingedrukt == false)
  {
    step1();
    delayMicroseconds(1000);
  }
  
  if (ingedrukt == true)
  {
    cstep = -1;
  }
}


void step1()
{
  //stepp
  switch (cstep)
  {
    case 0:
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, LOW);
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, HIGH);
      break;
    case 1:
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, LOW);
      digitalWrite(IN3, HIGH);
      digitalWrite(IN4, HIGH);
      break;
    case 2:
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, LOW);
      digitalWrite(IN3, HIGH);
      digitalWrite(IN4, LOW);
      break;
    case 3:
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, HIGH);
      digitalWrite(IN3, HIGH);
      digitalWrite(IN4, LOW);
      break;
    case 4:
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, HIGH);
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, LOW);
      break;
    case 5:
      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, HIGH);
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, LOW);
      break;
    case 6:
      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, LOW);
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, LOW);
      break;
    case 7:
      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, LOW);
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, HIGH);
      break;
    default:
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, LOW);
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, LOW);
      break;
  }
        cstep++;
    if (cstep == 8)
    {
      cstep = 0;
    }
}
