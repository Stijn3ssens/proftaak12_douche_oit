// include the library code:
#include <LiquidCrystal.h>

//Start de library met de juiste poorten
const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
const int vo = 9; //pot
String naam;
String woord;
String TotaleString;
char letters[50];
long timer = 0;
int debounce = 500;
int aanhetlezen = 0;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

//Bepaald de hoeveelheid kolommen en rijen
void setup() {
  lcd.begin(16, 2);
  Serial.begin(9600);
  pinMode(vo, INPUT);
  analogWrite(vo, 125);

}

//Wacht tot er een bericht komt
//Het bericht dat de arduino binnenkrijgt is de nieuwe naam
//Vervolgens laat de arduino op het lcd scherm 1 keer de naam zien van links naar rechts
void loop() {
  while (aanhetlezen != 2) {
    //Serial.println("Binnen");
    if (Serial.available() > 0)
    {
      char Lees = Serial.read();

      if (Lees != '\n') {
        if (Lees == '#') {
          aanhetlezen = 1;
        }
        else if (Lees == '%') {
          aanhetlezen = 2;
        }
        else if (aanhetlezen == 1) {
          TotaleString = TotaleString + Lees;
        }
      }
    }
  }
  naam = TotaleString;
  TotaleString = "";
  woord = "Welkom " + naam;
  woord.toCharArray(letters, 50);
  int woordlengte = (strlen(letters));
  lcd.setCursor(16, 0);
  for (int i = 0; i < woordlengte; i++) 
  {
    lcd.print(letters[i]);
  }
  for (int positionCounter = 0; positionCounter < (16 + woordlengte);) 
  {
    // scroll one position left:
    if (debounce < millis() - timer) 
    {
      lcd.scrollDisplayLeft(); 
      positionCounter++;
      timer = millis();
    }
  }
  aanhetlezen = 0;
  lcd.clear();
}
