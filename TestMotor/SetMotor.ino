//Dit draait de motor door de magneten te activeren in een bepaalde orde
int stepmotor(int motorInput1, int motorInput2, int motorInput3, int motorInput4, int cstep)
{
  //stepp
  switch (cstep)
  {
    case 0:
      SetMotor(motorInput1, motorInput2, motorInput3, motorInput4, LOW, LOW, LOW, HIGH);
      break;
    case 1:
      SetMotor(motorInput1, motorInput2, motorInput3, motorInput4, LOW, LOW, HIGH, HIGH);
      break;
    case 2:
      SetMotor(motorInput1, motorInput2, motorInput3, motorInput4, LOW, LOW, HIGH, LOW);
      break;
    case 3:
      SetMotor(motorInput1, motorInput2, motorInput3, motorInput4, LOW, HIGH, HIGH, LOW);
      break;
    case 4:
      SetMotor(motorInput1, motorInput2, motorInput3, motorInput4, LOW, HIGH, LOW, LOW);
      break;
    case 5:
      SetMotor(motorInput1, motorInput2, motorInput3, motorInput4, HIGH, HIGH, LOW, LOW);
      break;
    case 6:
      SetMotor(motorInput1, motorInput2, motorInput3, motorInput4, HIGH, LOW, LOW, LOW);
      break;
    case 7:
      SetMotor(motorInput1, motorInput2, motorInput3, motorInput4, HIGH, LOW, LOW, HIGH);
      break;
    default:
      SetMotor(motorInput1, motorInput2, motorInput3, motorInput4, LOW, LOW, LOW, LOW);
      break;
  }
  cstep++;
  if (cstep == 8)
  {
    cstep = 0;
  }
  return cstep;
}
//Dit draait de geselecteerde motor
void SetMotor(int motorInput1, int motorInput2, int motorInput3, int motorInput4, int motor1, int motor2, int motor3, int motor4)
{
  digitalWrite(motorInput1, motor1);
  digitalWrite(motorInput2, motor2);
  digitalWrite(motorInput3, motor3);
  digitalWrite(motorInput4, motor4);
}
