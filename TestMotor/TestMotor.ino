//Motor A poorten
const int IN1 = 2;
const int IN2 = 3;
const int IN3 = 5;
const int IN4 = 6;

//Motor B poorten
const int IN5 = 8;
const int IN6 = 9;
const int IN7 = 10;
const int IN8 = 11;

//Variabelen voor motoren
int Steps = 768; //4096 or 768
int cstep1 = 0;
int cstep2 = 0;
String TotaleString;
bool Draaien1 = false;
bool Draaien2 = false;
long timer1 = 0;
long timer2 = 0;
int interval1 = 0;
int interval2 = 0;
//interval moet tussen 0 en 5 zijn. 0 en 5 mogen ook!

//Zet de poorten op de juiste soort modus
//De seriele communicatie is gezet op 9600
void setup()
{
  Serial.begin(9600);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  pinMode(IN5, OUTPUT);
  pinMode(IN6, OUTPUT);
  pinMode(IN7, OUTPUT);
  pinMode(IN8, OUTPUT);
}

//Arrays voor de namen en snelheden
//Er kunnen momenteel 4 motoren tegelijk worden ontvangen in de lijst
String arrMotorNaam[] = {"", "", "", ""};
String arrMotorSnelheid[] = {"", "", "", ""};
int maxAmotor = 4; 

//Als er een bericht is ontvangen leest de arduino of er een bericht is met '#' en stopt met lezen bij '%'
//Voordat de snelheid wordt gegeven zit er een '_' dat de snelheid en naam van de motor apart houdt 
//Als er een '&' is betekent het dat er een extra motor wordt aangestuurd
//Uiteindelijk splitst het bericht en wordt het doorgegeven naar de motoren
void loop()
{
  if (Serial.available() > 1)
  {
    Serial.println("Aan het lezen");

    arrMotorNaam[0] = "";
    arrMotorNaam[1] = "";
    arrMotorSnelheid[0] = "";
    arrMotorSnelheid[1] = "";

    int aanhetlezen = 99;
    bool motorNaam = true;

    while (aanhetlezen != 100) {
      if (Serial.available() > 0)
      {
        char Lees = Serial.read();

        if (Lees != '\n') {
          if (Lees == '#') {
            aanhetlezen = 0;
            motorNaam = true;
          }
          else if (Lees == '%' || aanhetlezen > maxAmotor) {
            aanhetlezen = 100;
          }
          else if (Lees == '_') {
            motorNaam = false;
          }
          else if (Lees == '&') {
            aanhetlezen++;
            motorNaam = true;
          }
          else if (aanhetlezen <= maxAmotor)
          {
            if (motorNaam == true) {
              arrMotorNaam[aanhetlezen] = arrMotorNaam[aanhetlezen] + Lees;
            }
            else {
              arrMotorSnelheid[aanhetlezen] = arrMotorSnelheid[aanhetlezen] + Lees;
            }
          }
        }
      }
    }
//Zet de geselecteerde motoren aan met een bepaalde snelheid
    Serial.println("Zoeken");

    for (int i = 0; i < 4; i++) {

      Serial.println(arrMotorNaam[i]);
      Serial.println(arrMotorSnelheid[i]);

      if (arrMotorNaam[i] == "A1") {
        Draaien1 = true;
        interval1 = arrMotorSnelheid[i].toInt();
      }
      else if (arrMotorNaam[i] == "A0") {
        Draaien1 = false;
      }
      if (arrMotorNaam[i] == "B1") {
        Draaien2 = true;
        interval2 = arrMotorSnelheid[i].toInt();
      }
      else if (arrMotorNaam[i] == "B0") {
        Draaien2 = false;
      }
    }
  }
//Zorgt voor de snelheid waarop de motor draait
  if (Draaien1 == true && millis() - timer1 > interval1)
  {
    cstep1 = stepmotor(IN1, IN2, IN3, IN4, cstep1);
    timer1 = millis();
  }
  if (Draaien2 == true && millis() - timer2 > interval2)
  {
    cstep2 = stepmotor(IN5, IN6, IN7, IN8, cstep2);
    timer2 = millis();
  }
}
