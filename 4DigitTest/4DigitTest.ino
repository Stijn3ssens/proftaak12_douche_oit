/*
  Showing number 0-9 on a Common Anode 7-segment LED display
  Displays the numbers 0-9 on the display, with one second inbetween.
    A
   ---
  F |   | B
  | G |
   ---
  E |   | C
  |   |
   ---
    D
  This example code is in the public domain.
*/

// Pin 2-8 is connected to the 7 segments of the display.

int pinA = 2;
int pinB = 3;
int pinC = 4;
int pinD = 5;
int pinE = 6;
int pinF = 7;
int pinG = 9;
int D1 = 10;
int D2 = 11;
int D3 = 12;
int D4 = 13;
int h = HIGH;
int l = LOW;

int num1;
int num2;
int num3;
int num4;

// the setup routine runs once when you press reset:
void setup() {
  // initialize the digital pins as outputs.
  pinMode(pinA, OUTPUT);
  pinMode(pinB, OUTPUT);
  pinMode(pinC, OUTPUT);
  pinMode(pinD, OUTPUT);
  pinMode(pinE, OUTPUT);
  pinMode(pinF, OUTPUT);
  pinMode(pinG, OUTPUT);
  pinMode(D1, OUTPUT);
  pinMode(D2, OUTPUT);
  pinMode(D3, OUTPUT);
  pinMode(D4, OUTPUT);
  Serial.begin(9600);
}

int getal = 0000;

// the loop routine runs over and over again forever:
void loop() {

  splits(getal);
  
  showGetal(D1, num1);
  delay(1);
  showGetal(D2, num2);
  delay(1);
  showGetal(D3, num3);
  delay(1);
  showGetal(D4, num4);
  delay(1);
  if(getal >= 10000)
  {
    getal = 0000;
  }
}

void showGetal(int plaats, int getal)
{
  digitalWrite(D1, HIGH);
  digitalWrite(D2, HIGH);
  digitalWrite(D3, HIGH);
  digitalWrite(D4, HIGH);
  switch (getal)
  {
    case 0:
      Getl(plaats, h, h, h, h, h, h, l);
      break;
    case 1:
      Getl(plaats, l, h, h, l, l, l, l);
      break;
    case 2:
      Getl(plaats, h, h, l, h, h, l, h);
      break;
    case 3:
      Getl(plaats, h, h, h, h, l, l, h);
      break;
    case 4:
      Getl(plaats, l, h, h, l, l, h, h);
      break;
    case 5:
      Getl(plaats, h, l, h, h, l, h, h);
      break;
    case 6:
      Getl(plaats, h, l, h, h, h, h, h);
      break;
    case 7:
      Getl(plaats, h, h, h, l, l, l, l);
      break;
    case 8:
      Getl(plaats, h, h, h, h, h, h, h);
      break;
    case 9:
      Getl(plaats, h, h, h, h, l, h, h);
      break;
      default: 
      Getl(plaats, l, l, l ,l ,l ,l ,l);
  }
}

void Getl(int plaats, int a, int b, int c, int d, int e, int f, int g)
{
  digitalWrite(plaats, LOW);
  digitalWrite(pinA, a);
  digitalWrite(pinB, b);
  digitalWrite(pinC, c);
  digitalWrite(pinD, d);
  digitalWrite(pinE, e);
  digitalWrite(pinF, f);
  digitalWrite(pinG, g);
}



void splits(int getal)
{
  num4 = getal % 10;
  num3 = (getal / 10) % 10;
  num2 = (getal / 100) % 10;
  num1 = (getal / 1000) % 10;
  Serial.println(num4);
}
